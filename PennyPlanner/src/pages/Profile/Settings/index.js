import React from 'react'
import axios from 'axios'
import { Link, useNavigate, Outlet } from 'react-router-dom'
import '../../../assets/css files/profile.css'
import Sidebar from '../../../components/sidebar'

export default function Settings() {
    const exportExpenses = async () => {
        try {
            const response = await axios.get('https://penny-planner.eu/api/v1/expenses');
            const expenses = response.data.expenses;

            // Write expenses to a TXT file
            const blob = new Blob([JSON.stringify(expenses, null, 2)], { type: 'text/plain' });
            const url = window.URL.createObjectURL(blob);
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', 'expenses.txt');
            document.body.appendChild(link);
            link.click();
            link.parentNode.removeChild(link);
        } catch (error) {
            console.error('Error exporting expenses:', error);
        }
    };


    return (
        <>
            <div className="content_background overview">
                <div className="profile_circle_background">
                </div>

                <div className="overview-content__container">
                    <div className="profile-header__container andantedisplay-bold-black-48px">
                        <p>Settings</p>
                    </div>

                    <div className="profile-content__container">
                        <div className="config__container">
                            <div className="config-content__container">
                                <div className="config-content-top__container andantetext-bold-black-20px">
                                    <p>Categories</p>
                                    <p>{">"}</p>
                                </div>
                                <div className="config-content-bot__container andantetext-bold-black-20px" onClick={exportExpenses}>
                                    <p>Export Expenses</p>
                                    <p>{">"}</p>
                                </div>
                            </div>

                            <div className="config-content__container">
                                <div className="config-content-top__container andantetext-bold-black-20px">
                                    <p>Security and privacy</p>
                                    <p>{">"}</p>
                                </div>
                                <div className="config-content-bot__container andantetext-bold-black-20px">
                                    <p>Legal Notice</p>
                                    <p>{">"}</p>
                                </div>
                            </div>

                            <div className="config-content-more__container andantetext-bold-black-20px">
                                <p>More to come...</p>
                            </div>

                            <div className="settings-log-out__container andantetext-bold-black-20px">
                                <Link to="/" className="settings-log-out-button" style={{ textDecoration: "none", color: "#F30E0E"  }}>
                                    <p>Log out of your account</p>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Sidebar />
        </>
    )
}