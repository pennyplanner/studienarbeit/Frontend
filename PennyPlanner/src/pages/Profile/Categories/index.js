import React from 'react'
import { Link, useNavigate, Outlet } from 'react-router-dom'
import '../../../assets/css files/profile.css'
import Sidebar from '../../../components/sidebar'

export default function Categories() {
    return (
        <>
            <div className="content_background overview">
                <div className="profile_circle_background">
                </div>

                <div className="overview-content__container">
                    <div className="profile-header__container andantedisplay-bold-black-48px">
                        <p>Categories</p>
                    </div>

                    <div className="profile-content__container">

                        <div className="config__container">

                            <div className="categories-content__container" style={{marginBottom: "15vh"}}>
                                <div className="generated-categories-header andantetext-bold-blue-30px">
                                    <p>Generated Categories</p>
                                </div>
                                <div className="categories-content-top__container andantetext-bold-black-20px">
                                    <p>Healthcare</p>
                                    <p>{">"}</p>
                                </div>
                                <div className="categories-content-mid__container andantetext-bold-black-20px">
                                    <p>Transportation</p>
                                    <p>{">"}</p>
                                </div>
                                <div className="categories-content-mid__container andantetext-bold-black-20px">
                                    <p>Education</p>
                                    <p>{">"}</p>
                                </div>
                                <div className="categories-content-bot__container andantetext-bold-black-20px" style={{ color: "#656565" }}>
                                    <p>Show more</p>
                                    <p>{">"}</p>
                                </div>
                            </div>

                            <div className="categories-content__container" >
                                <div className="generated-categories-header andantetext-bold-blue-30px">
                                    <p>Created Categories</p>
                                </div>
                                <div className="categories-content-top__container andantetext-bold-black-20px">
                                    <p>Groceries</p>
                                    <p>{">"}</p>
                                </div>
                                <div className="categories-content-mid__container andantetext-bold-black-20px">
                                    <p>Vacation</p>
                                    <p>{">"}</p>
                                </div>
                                <div className="categories-content-bot__container andantetext-bold-black-20px" style={{ color: "#656565" }}>
                                    <p>Create new category</p>
                                    <p>{">"}</p>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>
            <Sidebar />
        </>
    )
}