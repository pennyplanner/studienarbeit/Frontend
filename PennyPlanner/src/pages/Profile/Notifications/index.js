import React from 'react'
import { Link, useNavigate, Outlet } from 'react-router-dom'
import '../../../assets/css files/profile.css'
import Sidebar from '../../../components/sidebar'
import BinIcon from '../../../assets/images/trash-bin.svg'

export default function Notifications() {
    return (
        <>
            <div className="content_background overview">
                <div className="profile_circle_background">
                </div>

                <div className="overview-content__container">
                    <div className="profile-header__container andantedisplay-bold-black-48px">
                        <p>Notifications</p>
                    </div>

                    <div className="profile-content__container" >


                        <div className="all-notifications__container">
                            <div className="notifications-logo__container andantedisplay-bold-black-48px" style={{ color: "#656565" }}>
                                <p>PennyPlanner</p>
                            </div>


                            <Notification text="You have 50€ left of your Food Budget" iconSrc={BinIcon} />
                            
                        </div>

                    </div>
                </div>

            </div>
            <Sidebar />
        </>
    )
}


const Notification = ({ text, iconSrc }) => {
    return (
        <div className="notification__container andantetext-bold-black-20px">
            <p>{text}</p>
            <img src={iconSrc} alt="" style={{ height: "60%" }} />
        </div>
    );
};