import React from 'react'
import { Link, useNavigate, Outlet } from 'react-router-dom'
import '../../../assets/css files/profile.css'
import Sidebar from '../../../components/sidebar'

export default function Configuration() {
    return (
        <>
            <div className="content_background overview">
                <div className="profile_circle_background">
                </div>

                <div className="overview-content__container">
                    <div className="profile-header__container andantedisplay-bold-black-48px">
                        <p>Configure Profile</p>
                    </div>

                    <div className="profile-content__container">
                        <div className="config__container">
                            <div className="config-content__container">
                                <div className="config-content-top__container andantetext-bold-black-20px">
                                    <p>Change name</p>
                                    <p>{">"}</p>
                                </div>
                                <div className="config-content-mid__container andantetext-bold-black-20px">
                                    <p>Change personal info</p>
                                    <p>{">"}</p>
                                </div>
                                <div className="config-content-bot__container andantetext-bold-black-20px" style={{color: "#F30E0E"}}>
                                    <p>Change password</p>
                                    <p>{">"}</p>
                                </div>
                            </div>

                            <div className="config-content__container">
                                <div className="config-content-top__container andantetext-bold-black-20px">
                                    <p>Virtual Accounts</p>
                                    <p>{">"}</p>
                                </div>
                                <div className="config-content-bot__container andantetext-bold-black-20px" style={{color: "#39536A"}}>
                                    <p>Add Bank Account</p>
                                    <p>{"+"}</p>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

            </div>
            <Sidebar />
        </>
    )
}