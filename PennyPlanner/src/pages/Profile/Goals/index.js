import React, { useState } from 'react'
import { Link, useNavigate, Outlet } from 'react-router-dom'
import '../../../assets/css files/profile.css'
import Sidebar from '../../../components/sidebar'

const GoalComponent = ({ goalName, startAmount, endAmount, goalType, endDate }) => {
    // Berechnung des Fortschritts in Prozent
    const progress = ((startAmount - endAmount) / startAmount) * 100;

    // Funktion zur Bestimmung des Zieltyps
    const getGoalType = () => {
        switch (goalType) {
            case 1:
                return "Goal Type 1";
            case 2:
                return "Goal Type 2";
            case 3:
                return "Goal Type 3";
            case 4:
                return "Goal Type 4";
            default:
                return "Unknown Goal Type";
        }
    };

    // Funktion zur Berechnung der verbleibenden Tage
    const calculateRemainingDays = () => {
        const today = new Date();
        const end = new Date(endDate);
        const diffTime = end - today;
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        return diffDays;
    };

    return (
        <div className="goal-content__container">
            <div className="goal-content-title__container">
                <p className='andantetext-bold-black-20px'>{goalName}</p>
                <p className='andantetext-regular-black-20px' style={{ color: "#656565" }}>{calculateRemainingDays()} days until next installment {">"}</p>
            </div>
            <div className="goal-content-target__container">
                <p className='andantetext-bold-blue-30px'>{endAmount} €&nbsp;</p>
                <p className='andantetext-regular-black-20px' style={{ color: "#656565" }}>left to cover</p>
            </div>
            <div className="goal-content-progress__container">
                <div className="goal-progress-bar-background__container">
                    <div className="goal-progress-bar__container" style={{ width: `${progress}%` }}>
                        {/* Fortschrittsbalken */}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default function Goals() {
    return (
        <>
            <div className="content_background overview">
                <div className="profile_circle_background">
                </div>

                <div className="overview-content__container">
                    <div className="profile-header__container andantedisplay-bold-black-48px">
                        <p>Goals</p>
                    </div>

                    <div className="profile-content__container">
                        <div className="config__container">
                            <div className="goal-content__container">
                                <div className="goal-content-title__container">
                                    <p className='andantetext-bold-black-20px'>Loan Repayment</p>
                                    <p className='andantetext-regular-black-20px' style={{ color: "#656565" }}>12 days until next installment {">"}</p>
                                </div>
                                <div className="goal-content-target__container">
                                    <p className='andantetext-bold-blue-30px'>461 €&nbsp;</p>
                                    <p className='andantetext-regular-black-20px' style={{ color: "#656565" }}>left to cover</p>
                                </div>
                                <div className="goal-content-progress__container">
                                    <div className="goal-progress-bar-background__container">
                                        <div className="goal-progress-bar__container">

                                        </div>
                                    </div>

                                </div>
                            </div>

                            <GoalComponent
                                goalName="Loan Repayment"
                                startAmount={1000}
                                endAmount={539}
                                goalType={2}
                                endDate="2024-05-30"
                            />

                            <div className="config-content__container">

                            </div>

                            <div className="config-content__container">

                            </div>

                            <div className="config-content__container">

                            </div>


                        </div>


                    </div>
                </div>

            </div>

            <Sidebar />


        </>
    )
}