import React from 'react'
import { Link, useNavigate, Outlet } from 'react-router-dom'
import '../../../assets/css files/profile.css'
import Sidebar from '../../../components/sidebar'

export default function Overview() {
    return (
        <>
            <div className="content_background overview">
                <div className="profile_circle_background">
                </div>

                <div className="overview-content__container">
                    <div className="profile-header__container andantedisplay-bold-black-48px">
                        <p>Financial Overview</p>
                    </div>

                    <div className="profile-content__container">

                        <div className="config__container">
                            <div className="goal-content__container">
                                <div className="goal-content-title__container">
                                    <p className='andantetext-bold-black-20px'>Statistics</p>
                                    <p className='andantetext-regular-black-20px' style={{ color: "#656565" }}>24 days until the end of the month {">"}</p>
                                </div>
                                <div className="goal-content-target__container">
                                    <p className='andantetext-bold-blue-30px'>461 €&nbsp;</p>
                                    <p className='andantetext-regular-black-20px' style={{ color: "#656565" }}>available from salary</p>
                                </div>
                                <div className="goal-content-progress__container">
                                    <div className="goal-progress-bar-background__container">
                                        <div className="goal-progress-bar__container">

                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div className="overview-accounts__container">
                                <div className="overview-accounts-header andantetext-bold-blue-30px">
                                    <p>Daily Accounts</p>
                                </div>
                                <div className="overview-accounts-content__container andantetext-bold-black-20px">
                                    <div className="overview-accounts-content-acc__container">
                                        <p>All bookings</p>
                                        <p style={{ color: "green" }}>1846,70 €</p>
                                    </div>
                                    <div className="overview-accounts-content-acc__container">
                                        <p>Checking account</p>
                                        <p style={{ color: "green" }}>1826,20 €</p>
                                    </div>
                                    <div className="overview-accounts-content-acc__container">
                                        <p>PayPal</p>
                                        <p style={{ color: "green" }}>5,50 €</p>
                                    </div>
                                    <div className="overview-accounts-content-acc__container" style={{ borderBottom: "none" }}>
                                        <p>Cash</p>
                                        <p style={{ color: "green" }}>15 €</p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <Sidebar />
        </>
    )
}