import React from 'react'
import { Link, useNavigate, Outlet } from 'react-router-dom'
import '../../../assets/css files/profile.css'
import Sidebar from '../../../components/sidebar'

export default function MonthlyExpenses() {
    return (
        <>
            <div className="content_background overview">
                <div className="profile_circle_background">
                </div>

                <div className="overview-content__container">
                    <div className="profile-header__container andantedisplay-bold-black-48px">
                        <p>Monthly Expenses</p>
                    </div>

                    <div className="profile-content__container">

                        <div className="config__container">

                            <div className="total-expenses-content__container">
                                <div className="total-expenses-content-title__container">
                                    <p className='andantetext-bold-black-20px'>Total Expenses</p>
                                    <p className='andantetext-regular-black-20px' style={{ color: "#656565" }}>Details {">"}</p>
                                </div>
                                <div className="total-expenses-content-target__container">
                                    <p className='andantetext-bold-blue-30px'>Ø 461 €&nbsp;</p>
                                    <p className='andantetext-regular-black-30px'> / Month</p>
                                </div>
                            </div>

                            <div className="monthly-expense-content__container">

                                <div className="monthly-expense-content-title__container andantetext-bold-blue-30px">
                                    <p>Monthly {"("}2{")"}</p>
                                </div>
                                <div className="monthly-expense-content-top__container andantetext-bold-black-20px">
                                    <p>John Doe</p>
                                    <p style={{ color: "#656565" }}>Rent</p>
                                    <p>{">"}</p>
                                </div>
                                <div className="monthly-expense-content-bot__container andantetext-bold-black-20px">
                                    <p>VBK</p>
                                    <p style={{ color: "#656565" }}>Bus & Train</p>
                                    <p>{">"}</p>
                                </div>
                            </div>

                            <div className="config-content__container">
                                <div className="config-content-top__container andantetext-bold-black-20px" style={{ color: "#39536A" }}>
                                    <p>Add monthly expense</p>
                                    <p>{">"}</p>
                                </div>
                                <div className="config-content-bot__container andantetext-bold-black-20px" style={{ color: "#39536A" }}>
                                    <p>Add monthly expense</p>
                                    <p>{">"}</p>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <Sidebar />
        </>
    )
}